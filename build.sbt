scalaVersion in ThisBuild := "2.12.6"
organization in ThisBuild := "ru.tinkoff.fintech"

// https://docs.scala-lang.org/overviews/compiler-options/index.html
val scalacCompileOpts =  Seq(
  "-feature",
  "-unchecked",
  // "-deprecation:false", // uncomment if you *must* use deprecated apis
  "-Xfatal-warnings",
  "-Ywarn-value-discard",
  "-Xlint:unsound-match"
)

lazy val assignment = (project in file("assignment"))
  .settings(
    name := "lesson5-assignment",
    libraryDependencies ++= Seq(
      "com.typesafe" % "config" % "1.3.3",
      "com.iheart" %% "ficus" % "1.4.3",
      "org.slf4j" % "slf4j-api" % "1.7.25",
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
      "com.github.t3hnar" %% "scala-bcrypt" % "4.0",

      "org.scalatest" %% "scalatest" % "3.0.3" % Test,
      
      scalacOptions in(Compile, compile) ++= scalacCompileOpts
    )
  )

lazy val practice = (project in file("practice"))
  .settings(
    name := "lesson5-practice",
    scalacOptions in(Compile, compile) ++= scalacCompileOpts
  )