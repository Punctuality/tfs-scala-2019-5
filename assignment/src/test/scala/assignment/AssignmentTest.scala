package assignment

import java.util.concurrent.ThreadLocalRandom
import java.util.concurrent.atomic.{AtomicBoolean, AtomicInteger}

import bcrypt.AsyncBcryptImpl
import com.typesafe.config.{Config, ConfigFactory}
import org.scalatest.Matchers._
import org.scalatest._
import util.CounterException

import scala.concurrent._
import scala.concurrent.duration._
import scala.util.Random._

class AssignmentTest extends AsyncFlatSpec {

  val config: Config = ConfigFactory.load()
  val credentialStore: ConfigCredentialStore = new ConfigCredentialStore(config)
  val reliableBcrypt: AsyncBcryptImpl = new AsyncBcryptImpl
  val assignment: Assignment = new Assignment(reliableBcrypt, credentialStore)(ExecutionContext.global)

  import assignment._

  behavior of "AsyncBcryptImpl"

  it should "hashes should work properly" in {
    val string: String = "TestString"
    val hash: Future[String] = reliableBcrypt.hash(string)
    val result: Future[Boolean] = hash.flatMap(reliableBcrypt.verify(string, _))
    result.map {
      _ shouldBe true
    }
  }

  behavior of "verifyCredentials"

  it should "return true for valid user-password pair" in {
    verifyCredentials("winnie", "pooh").map { result =>
      result shouldBe true
    }
  }

  it should "return false if user does not exist in store" in
    verifyCredentials("octopus", "dad").map {
      _ shouldBe false
    }
  it should "return false for invalid password" in
    verifyCredentials("poodle", "kitty").map {
      _ shouldBe false
    }

  behavior of "withCredentials"

  it should "execute code block if credentials are valid" in {
    val result: Future[String] = withCredentials("poodle", "doge")("Хороший")
    result.map {
      _ shouldBe "Хороший"
    }
  }
  it should "not execute code block if credentials are not valid" in {
    val checker = new AtomicBoolean(true)
    val result: Future[String] = withCredentials("Mom", "BeStMoMeVeR")({
      checker.set(false)
      "Плохой"
    })
    result.recover {
      case _ : InvalidCredentialsException if checker.get => "Хороший"
      case _ => "Все еще плохой"
    }.map(_ shouldBe "Хороший")
  }

  behavior of "hashPasswordList"

  it should "return matching password-hash pairs" in {
    val passwords: Seq[String] = Seq("pooh", "doge")
    val hashes: Future[Seq[String]] = hashPasswordList(passwords).map(seq => seq.map {
      _._2
    })
    hashes.flatMap { seq =>
      Future.sequence(seq.indices.map {
        ind => reliableBcrypt.verify(passwords(ind), seq(ind))
      })
    }.map { seq =>
      seq.forall(bool => bool) shouldBe true
    }
  }

  behavior of "findMatchingPassword"

  it should "return matching password from the list" in {
    val passwords: Seq[String] = Seq("pooh", "doge")
    val hash = "$2a$10$C9.CqKxHJKepdSMUkUdVKeVvt5wo/9ZQdHqGZ23A6XLOM5YDMOrqC"
    findMatchingPassword(passwords, hash).map {
      _ shouldBe Some("doge")
    }
  }
  it should "return None if no matching password is found" in {
    val passwords: Seq[String] = Seq("pooh", "kitty")
    val hash = "$2a$10$C9.CqKxHJKepdSMUkUdVKeVvt5wo/9ZQdHqGZ23A6XLOM5YDMOrqC"
    findMatchingPassword(passwords, hash).map {
      _ shouldBe None
    }
  }

  behavior of "withRetry"


  it should "return result on passed future's success" in {
    val testFuture: Future[Int] = Future((1 to 1000).toList.sum)
    withRetry(testFuture, 5).map {
      _ should equal((1 to 1000).sum)
    }
  }

  it should "not execute more than specified number of retries" in {

    val n_retries: Int = 30

    val atomicCounter = new AtomicInteger(0)

    withRetry(Future.failed {
      CounterException(atomicCounter.addAndGet(1))
    }, n_retries).recover {
      case _: CounterException => atomicCounter.get shouldBe n_retries
    }
  }

  it should "not execute unnecessary retries" in {
    val atomicCounter = new AtomicInteger(0)
    val n_retries = 30
    val limit = 15

    withRetry(Future {
      if (atomicCounter.get < limit) {
        throw CounterException(atomicCounter.addAndGet(1))
      } else {
        "We're cool!"
      }
    }, n_retries)
      .recover {
        case exp: CounterException => exp.getCount == 1
      }
      .map { _ =>
        atomicCounter.get shouldBe limit
      }
  }

  it should "return the first error, if all attempts fail" in {

    val atomicCounter = new AtomicInteger(0)

    withRetry(Future.failed {
      CounterException(atomicCounter.addAndGet(1))
    }, 3)
      .recover {
        case exp: CounterException => exp.getCount == 1
      }
      .map { result =>
        result shouldBe true
      }
  }

  behavior of "withTimeout"

  it should "return result on passed future success" in {
    val duration: FiniteDuration = 5 second
    val result: Future[Int] = withTimeout(Future {
      (1 to 666).sum
    }, duration)
    result.map {
      _ shouldBe (1 to 666).sum
    }
  }
  it should "return result on passed future failure" in {
    val duration: FiniteDuration = 5 second
    val result: Future[Int] = withTimeout(Future {
      throw new RuntimeException()
    }, duration)
    result.recover {
      case _: RuntimeException => 1
      case _: TimeoutException => 2
      case _ => 0
    }.map {
      _ shouldBe 1
    }
  }
  it should "complete on never-completing future" in {
    val duration: FiniteDuration = 1 second
    val futureToTest = Promise[Int]().future
    val result: Future[Int] = withTimeout(futureToTest, duration)
    result.recover {
      case _: TimeoutException => 1
      case _ => 0
    }.map {
      _ shouldBe 1
    }
  }

  behavior of "hashPasswordListReliably"
  val assignmentFlaky = new Assignment(new FlakyBcryptWrapper(reliableBcrypt), credentialStore)(ExecutionContext.global)

  it should "return password-hash pairs for successful hashing operations" in {


    def unfold[A, B](start: A)(expand: A => Option[(A, B)]): Stream[B] =
      expand(start).map { case (a, b) => b #:: unfold(a)(expand) }.getOrElse(Stream())

    val passwords = unfold(1)(len => if (len <= 20) Some {
      (
        len + 1,
        ThreadLocalRandom.current().nextString(len)
      )
    } else None)

    val hashes: Future[Seq[(String, String)]] = assignmentFlaky.hashPasswordListReliably(
      passwords, 10, 2 second)

    hashes.flatMap(seq => Future.traverse(seq)(p => reliableBcrypt.verify(p._1, p._2)
    )).map {
      _.forall(bool => bool) shouldBe true
    } // интересно что _.forall(_) IDEA не понимает.
  }
}
