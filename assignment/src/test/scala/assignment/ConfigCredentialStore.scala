package assignment

import com.typesafe.config.Config
import net.ceedubs.ficus.Ficus._
import store.AsyncCredentialStore

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ConfigCredentialStore(config: Config) extends AsyncCredentialStore {
  private val credentialMap: Map[String, String] = config.as[Map[String, String]]("credentials")

  /**
    * возвращает хеш пользовательского пароля
    */
  override def find(user: String): Future[Option[String]] = Future(credentialMap.find(_._1.equals(user)).map(_._2))
}