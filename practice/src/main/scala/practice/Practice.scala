package practice

import java.io.Closeable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration._
import scala.io.{BufferedSource, Source}

case class Bucket(min: Int, max: Int, count: Int)

case class Stats(lineCount: Int,
                 minLineLength: Int,
                 averageLineLength: Int,
                 maxLineLength: Int,
                 buckets: Seq[Bucket])


object Practice extends App {
  private val defaultFileName = "in.txt"

  private def getSource(fileName: String): BufferedSource =
    Source.fromResource(fileName)

  private def read(in: BufferedSource): Future[Iterator[String]] =
    Future(blocking(in.getLines()))

  def asyncWithResource[R <: Closeable, T](resource: R)(block: R => Future[T]): Future[T] =
    block(resource).andThen { case _ => resource.close() }

  def asyncCountLines: Future[Int] = {
    val iterator: Future[Seq[String]] = asyncWithResource(getSource(defaultFileName)) { in => read(in).map(_.toList) }
    iterator.map(iter => iter.count(_ => true))
  }

  def asyncLineLengths: Future[Seq[(String, Int)]] = {
    val iterator: Future[Seq[String]] = asyncWithResource(getSource(defaultFileName)) { in => read(in).map(_.toList) }
    iterator.map(iter => iter.map(line => (line, line.length())))
  }

  def asyncTotalLength: Future[Int] =
    asyncLineLengths.map(seq => seq.foldRight(0)((pair, counter) => counter + pair._2))

  def countShorterThan(maxLength: Int): Future[Int] =
    asyncLineLengths.map(seq => seq.count(pair => pair._2 < maxLength))

  def printWithDelay(delay: FiniteDuration, s: String): Unit = {
    Thread.sleep(delay.length)
    println(s"Line: $s")
  }

  def sleepSort: Future[Unit] = {
    val lines: Future[Seq[(String, Int)]] = asyncLineLengths
    lines.map(seq => seq.foreach(pair => new Thread() {
      override def run(): Unit = {
        printWithDelay((pair._2 * 10).millis, pair._1)
      }
    }.start()))
  }

  sleepSort.onSuccess({case _ => println("Success")})

  Thread.sleep(5000)
}
