package util

final case class CounterException(private val count: Int = 0,
                                  private val cause: Throwable = None.orNull)
  extends Exception(s"Attempt #${count.toString}", cause){

  override def getMessage: String = count.toString
  def getCount: Int = count
}
