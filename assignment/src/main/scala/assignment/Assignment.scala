package assignment

import bcrypt.AsyncBcrypt
import com.typesafe.scalalogging.StrictLogging
import store.AsyncCredentialStore
import util.{Cancellable, Scheduler}

import scala.concurrent._
import scala.concurrent.duration._
import scala.util.{Failure, Success, Try}

class Assignment(bcrypt: AsyncBcrypt, credentialStore: AsyncCredentialStore)
                (implicit executionContext: ExecutionContext) extends StrictLogging {

  /**
    * проверяет пароль для пользователя
    * возвращает Future со значением false:
    *   - если пользователь не найден
    *   - если пароль не подходит к хешу
    */
  def verifyCredentials(user: String, password: String): Future[Boolean] =
    credentialStore.find(user) flatMap {
      case Some(hash) => bcrypt.verify(password, hash)
      case None => Future.successful(false)
    }


  /**
    * выполняет блок кода, только если переданы верные учетные данные
    * возвращает Future c ошибкой InvalidCredentialsException, если проверка не пройдена
    */
  def withCredentials[A](user: String, password: String)(block: => A): Future[A] =
    verifyCredentials(user, password).flatMap(if (_) {
      Future(block)
    } else {
      Future.failed(new InvalidCredentialsException())
    })

  /**
    * хеширует каждый пароль из списка и возвращает пары пароль-хеш
    */
  def hashPasswordList(passwords: Seq[String]): Future[Seq[(String, String)]] =
    Future.traverse(passwords)(password => bcrypt.hash(password).map(password -> _))

  /**
    * проверяет все пароли из списка против хеша, и если есть подходящий - возвращает его
    * если подходит несколько - возвращается любой
    */
  def findMatchingPassword(passwords: Seq[String], hash: String): Future[Option[String]] =
    Future.traverse(passwords)(bcrypt.verify(_, hash)).map {
      _.zip(passwords).find(_._1).map {
        _._2
      }
    }

  /**
    * логирует начало и окончание выполнения Future, и продолжительность выполнения
    */
  def withLogging[A](tag: String)(f: => Future[A]): Future[A] = {
    val startTime = System.currentTimeMillis()
    f andThen { case _ =>
      val endTime = System.currentTimeMillis()
      logger.info(s"Start: $startTime End: $endTime Execution Time: ${endTime - startTime}")
    }
  }

  /**
    * пытается повторно выполнить f retries раз, до первого успеха
    * если все попытки провалены, возвращает первую ошибку
    *
    * Важно: f не должна выполняться большее число раз, чем необходимо
    */

  def withRetry[A](f: => Future[A], retries: Int): Future[A] = {
    assert(retries > 0)
    f.recoverWith {
      case exp => withRetry(f, retries - 1) recoverWith { case _ => Future.failed(exp) }
    }
  }

  /**
    * по истечению таймаута возвращает Future.failed с java.util.concurrent.TimeoutException
    */
  def withTimeout[A](f: Future[A], timeout: FiniteDuration): Future[A] = {
    val failurePromise: Promise[A] = Promise[A]()
    val timeLimit: Cancellable = Scheduler.executeAfter(timeout)(failurePromise.failure {
        new TimeoutException()
      })
    val resultFuture: Future[A] = Future.firstCompletedOf(Seq(f, failurePromise.future))
    resultFuture onComplete (_ => timeLimit.cancel())
    resultFuture
  }

  /**
    * делает то же, что и hashPasswordList, но дополнительно:
    *   - каждая попытка хеширования отдельного пароля выполняется с таймаутом
    *   - при ошибке хеширования отдельного пароля, попытка повторяется в пределах retries (свой на каждый пароль)
    *   - возвращаются все успешные результаты
    */
  def hashPasswordListReliably(passwords: Seq[String], retries: Int, timeout: FiniteDuration): Future[Seq[(String, String)]] = {

    def reliableHash(password: String, retries: Int, timeout: FiniteDuration): Future[Try[(String, String)]] =
      withRetry(withTimeout(bcrypt.hash(password).map(password -> _), timeout), retries).map(Success(_)).
        recover{case exp => Failure(exp)}

    Future.traverse(passwords)(reliableHash(_, retries, timeout)).map(_.filter(_.isSuccess).map(_.get))
  }
}
